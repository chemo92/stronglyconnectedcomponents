package com.progcraft.logic.dfs;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

import com.progcraft.model.Graph;

public class DepthFirstSearchTest {

	private DepthFirstSearch target;
	
	@Test
	public void searchTestOneVertexGraph() {
		target = new DepthFirstSearch(buildGraph(new int[]{1, 1}));
		target.search();
		Map<Integer, Integer> result = target.getVerteciesFinishingTime();
		assertEquals(1, result.size());
		assertEquals(new Integer(1), result.get(1));
	}
	
	@Test
	public void seatchTestTwoConnectedVerteciesGraph() {
		target = new DepthFirstSearch(buildGraph(new int[]{1, 2}));
		target.search();
		Map<Integer, Integer> result = target.getVerteciesFinishingTime();
		assertEquals(2, result.size());
		assertEquals(new Integer(2), result.get(1));
		assertEquals(new Integer(1), result.get(2));
	}

	@Test
	public void searchTestThreeConnectedVerticesGraph() {
		target = new DepthFirstSearch(buildGraph(new int[]{1, 2, 2, 3}));
		target.search();
		Map<Integer, Integer> result = target.getVerteciesFinishingTime();
		assertEquals(3, result.size());
		assertEquals(new Integer(3), result.get(1));
		assertEquals(new Integer(2), result.get(2));
		assertEquals(new Integer(1), result.get(3));
	}
	
	@Test
	public void searchTestCircleGraph() {
		target = new DepthFirstSearch(buildGraph(new int[]{1, 2, 2, 3, 3, 1}));
		target.search();
		Map<Integer, Integer> result = target.getVerteciesFinishingTime();
		assertEquals(3, result.size());
		assertEquals(new Integer(2), result.get(1));
		assertEquals(new Integer(1), result.get(2));
		assertEquals(new Integer(3), result.get(3));
	}
	
	@Test
	public void searchTestOneVertexWithTwoOutgoingEdges() {
		target = new DepthFirstSearch(buildGraph(new int[]{1, 2, 1, 3}));
		target.search();
		Map<Integer, Integer> result = target.getVerteciesFinishingTime();
		assertEquals(3, result.size());
		assertEquals(new Integer(3), result.get(1));
		assertEquals(new Integer(2), result.get(2));
		assertEquals(new Integer(1), result.get(3));
	}
	
	@Test
	public void searchTestWithTwoParallelBrunches() {
		target = new DepthFirstSearch(buildGraph(new int[]{1, 2, 2, 3, 2, 4, 3, 5}));
		target.search();
		Map<Integer, Integer> result = target.getVerteciesFinishingTime();
		assertEquals(5, result.size());
		assertEquals(new Integer(5), result.get(1));
		assertEquals(new Integer(4), result.get(2));
		assertEquals(new Integer(3), result.get(3));
		assertEquals(new Integer(2), result.get(4));
		assertEquals(new Integer(1), result.get(5));
	}
	
	@Test
	public void searchTestWithTwoParallelBrunchesConnectedInTheEnd() {
		target = new DepthFirstSearch(buildGraph(new int[]{1, 2, 2, 3, 2, 4, 3, 5, 4, 5}));
		target.search();
		Map<Integer, Integer> result = target.getVerteciesFinishingTime();
		assertEquals(5, result.size());
		assertEquals(new Integer(5), result.get(1));
		assertEquals(new Integer(4), result.get(2));
		assertEquals(new Integer(3), result.get(3));
		assertEquals(new Integer(2), result.get(4));
		assertEquals(new Integer(1), result.get(5));
	}
	
	@Test
	public void searchTestWithTwoConnectedCircles() {
		target = new DepthFirstSearch(buildGraph(new int[]{1, 2, 2, 4, 4, 3, 3, 1, 4, 5, 5, 7, 7, 6, 6, 4}));
		target.search();
		Map<Integer, Integer> result = target.getVerteciesFinishingTime();
		assertEquals(7, result.size());
		assertEquals(new Integer(2), result.get(1));
		assertEquals(new Integer(1), result.get(2));
		assertEquals(new Integer(3), result.get(3));
		assertEquals(new Integer(5), result.get(4));
		assertEquals(new Integer(4), result.get(5));
		assertEquals(new Integer(6), result.get(6));
		assertEquals(new Integer(7), result.get(7));
	}
	
	private Graph buildGraph(int[] vertecies) {
		Graph graph = new Graph();
		for(int i = 0; i < vertecies.length; i += 2) {
			graph.addEdge(vertecies[i], vertecies[i + 1]);
		}
		return graph;
	}
}
