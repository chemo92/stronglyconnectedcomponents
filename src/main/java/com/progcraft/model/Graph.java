package com.progcraft.model;

import java.util.HashMap;
import java.util.Map;

public class Graph {

	private Map<Integer, Vertex> vertices;

	public Graph() {
		vertices = new HashMap<>();
	}
	
	public Map<Integer, Vertex> getVertices() {
		return vertices;
	}

	public void setVertices(Map<Integer, Vertex> vertices) {
		this.vertices = vertices;
	}
	
	public boolean isVertexExists(Integer vertexNumber) {
		return vertices.containsKey(vertexNumber);
	}
	
	public Vertex getVertex(Integer vertexNumber) {
		return vertices.get(vertexNumber);
	}
	
	public int getVerteciesCount() {
		return vertices.size();
	}
	
	public void addEdge(Integer tail, Integer head) {
		Vertex vertex = createVertexIfNotExists(tail);
		vertex.addHeadVertex(head);
		vertex = createVertexIfNotExists(head);
		vertex.addTailVertex(tail);
	}
	
	private Vertex createVertexIfNotExists(Integer vertexNumber) {
		Vertex vertex = null;
		if((vertex = vertices.get(vertexNumber)) == null) {
			vertex = new Vertex(vertexNumber);
			vertices.put(vertexNumber, vertex);
		}
		return vertex;
	}
}
