package com.progcraft.logic.dfs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.progcraft.model.Graph;
import com.progcraft.model.Vertex;

public class DepthFirstSearch {

	protected Set<Integer> visitedVertecies;
	private Graph graph;
	private int currentLeader;
	private int currentFinishingTime;
	private Map<Integer, Integer> verteciesOrdering;
	private Map<Integer, Integer> verteciesFinishingTime; 
	private Map<Integer, Integer> stronglyConnectedComponents;

	public DepthFirstSearch(Graph graph) {
		this.graph = graph;
	}
	
	public DepthFirstSearch(Graph graph, Map<Integer, Integer> ordering) {
		this.graph = graph;
		this.verteciesOrdering = ordering;
	}
	
	public Map<Integer, Integer> getVerteciesFinishingTime() {
		return verteciesFinishingTime;
	}

	public Map<Integer, Integer> getStronglyConnectedComponents() {
		return stronglyConnectedComponents;
	}
	
	public void search() {
		currentFinishingTime = 1;
		verteciesFinishingTime = new HashMap<>();
		visitedVertecies = new HashSet<>();
		stronglyConnectedComponents = new HashMap<>();
		
		for(int i = graph.getVerteciesCount(); i >= 1; i--) {
			int currentVertexNumber = getNextVertexNumber(i);
			if(!visitedVertecies.contains(currentVertexNumber)) {
				Vertex currentVertex = graph.getVertex(currentVertexNumber);
				currentLeader = currentVertex.getVertexNumber();
				stronglyConnectedComponents.put(currentLeader, 0);
				dfs(currentVertex);
			}
		}
	}
	
	private Integer getNextVertexNumber(int index) {
		Integer vertexNumber = null;
		if(verteciesOrdering != null) {
			vertexNumber = graph.getVertex(verteciesOrdering.get(index)).getVertexNumber();
		} else {
			vertexNumber = graph.getVertex(index).getVertexNumber();
		}
		return vertexNumber;
	}
	
	private void dfs(Vertex currentVertex) {
		addVertexToVisited(currentVertex);
		stronglyConnectedComponents.put(currentLeader, stronglyConnectedComponents.get(currentLeader) + 1);
		Integer headVertexNumber = getNextVertexNumberToVisit(currentVertex);
		while(headVertexNumber != null) {				
			Vertex headVertex = graph.getVertex(headVertexNumber);
			addVertexToVisited(headVertex);
			dfs(headVertex);
			headVertexNumber = getNextVertexNumberToVisit(currentVertex);
		}
		addTraversedVertexDFSResult(currentVertex);
	}
	
	protected Integer getNextVertexNumberToVisit(Vertex vertex) {
		return vertex.getHeadVertices().stream()
				.filter(currentVertex -> !visitedVertecies.contains(currentVertex))
				.findFirst().orElse(null);
	}
	
	private void addVertexToVisited(Vertex vertex) {
		visitedVertecies.add(vertex.getVertexNumber());
	}
	
	private void addTraversedVertexDFSResult(Vertex vertex) {
		verteciesFinishingTime.put(currentFinishingTime++, vertex.getVertexNumber());
	}
}
