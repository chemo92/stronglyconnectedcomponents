package com.progcraft.logic;

import java.util.Map;

import com.progcraft.logic.dfs.DepthFirstSearch;
import com.progcraft.logic.dfs.ReversedDepthFirstSearch;
import com.progcraft.model.Graph;

public class KosarajusAlgorithm {

	public static Map<Integer, Integer> run(Graph graph) {
		long startTime = System.currentTimeMillis();
		System.out.println("start");
		ReversedDepthFirstSearch reversedDepthFirstSearch = new ReversedDepthFirstSearch(graph);
		reversedDepthFirstSearch.search();
		Map<Integer, Integer>  newVerteciesOrdering = reversedDepthFirstSearch.getVerteciesFinishingTime();
		DepthFirstSearch depthFirstSearch = new DepthFirstSearch(graph, newVerteciesOrdering);
		depthFirstSearch.search();
		System.out.println((System.currentTimeMillis() - startTime) / 1000.0);
		return depthFirstSearch.getStronglyConnectedComponents();
	}
}
