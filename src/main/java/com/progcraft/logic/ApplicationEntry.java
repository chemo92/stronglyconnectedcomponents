package com.progcraft.logic;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import com.progcraft.model.Graph;

public class ApplicationEntry {

	private static final String FILE_NAME = "SCC.txt";
	
	public static void main(String[] args) throws IOException {
		GraphReader graphReader = new GraphReader();
		Graph graph = graphReader.read(FILE_NAME);
		Map<Integer, Integer> scc = KosarajusAlgorithm.run(graph);
		Collection<Integer> sccSizes = scc.values();
		sccSizes.stream().sorted().forEach(sccSize -> System.out.println(sccSize));
	}
}
